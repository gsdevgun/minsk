package software.measurement.soen6611.minsk.maths;

public class Sgn {
	//applied Singleton pattern
	private static Sgn instance = new Sgn();
	private Sgn()
	{
		
	}
	
	public static Sgn getInstance()
	{
		return instance;
	}
	
	// Sign Function.
	public double checkSignum(double a)
	{	
		if(a<0)
			return -1.0;
		else if(a>0)
			return 1.0;
		else
			return 0.0;
		
	}

}
