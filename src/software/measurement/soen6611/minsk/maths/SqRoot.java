package software.measurement.soen6611.minsk.maths;

public class SqRoot {
	//applied Singleton pattern.
	private static SqRoot instance = new SqRoot();
	private SqRoot()
	{
		
	}
	
	public static SqRoot getInstance()
	{
		return instance;
	}
	
	// babylonian's method
		public double SquareRoot(double N) {

			double squareRoot = 0;
			double number;
			double lastRoot = 0.0;
			
			if(N==1)
			{
				return 1.0;
			}
			
			number = N/2;
			while (true) {
				squareRoot = ((number + N / number) / 2.0);
				number = squareRoot;
				
				if (lastRoot == squareRoot)
					break;
					
				lastRoot = squareRoot;
				
			}
			return squareRoot;
		}

}
