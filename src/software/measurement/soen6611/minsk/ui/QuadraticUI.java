package software.measurement.soen6611.minsk.ui;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import software.measurement.soen6611.minsk.Prime;
import software.measurement.soen6611.minsk.QuadraticSC;
import software.measurement.soen6611.minsk.main.MINSK;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class QuadraticUI {

	public JFrame frame;
	private JTextField tfa;
	private JTextField tfb;
	private JTextField tfc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					QuadraticUI window = new QuadraticUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public QuadraticUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 493, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("a:");
		lblNewLabel.setBounds(30, 86, 69, 20);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("b:");
		lblNewLabel_1.setBounds(166, 86, 69, 20);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblC = new JLabel("c:");
		lblC.setBounds(324, 86, 69, 20);
		frame.getContentPane().add(lblC);
		
		JButton btnNewButton = new JButton("Solve");
		
		btnNewButton.setBounds(30, 122, 115, 29);
		frame.getContentPane().add(btnNewButton);
		
		tfa = new JTextField();
		tfa.setBounds(53, 83, 60, 26);
		frame.getContentPane().add(tfa);
		tfa.setColumns(10);
		
		tfb = new JTextField();
		tfb.setColumns(10);
		tfb.setBounds(195, 83, 60, 26);
		frame.getContentPane().add(tfb);
		
		tfc = new JTextField();
		tfc.setColumns(10);
		tfc.setBounds(351, 83, 60, 26);
		frame.getContentPane().add(tfc);
		
		JLabel lblEnterTheCoefficients = new JLabel("Enter the coefficients of a quadratic equation of form:");
		lblEnterTheCoefficients.setBounds(10, 16, 403, 20);
		frame.getContentPane().add(lblEnterTheCoefficients);
		
		JLabel lblAxBx = new JLabel("ax^2 + bx + c=0, a != 0 and a+b+c = Prime Number");
		lblAxBx.setBounds(10, 41, 431, 29);
		frame.getContentPane().add(lblAxBx);
		
		JLabel lblRoot = new JLabel("Root 1:");
		lblRoot.setBounds(30, 162, 69, 20);
		frame.getContentPane().add(lblRoot);
		
		JLabel lblRoot_1 = new JLabel("Root 2:");
		lblRoot_1.setBounds(30, 190, 69, 20);
		frame.getContentPane().add(lblRoot_1);
		
		final JLabel rootresult1 = new JLabel("");
		rootresult1.setBounds(94, 162, 261, 20);
		frame.getContentPane().add(rootresult1);
		
		final JLabel rootresult2 = new JLabel("");
		rootresult2.setBounds(94, 190, 299, 20);
		frame.getContentPane().add(rootresult2);
		
		JButton btnNewButton_1 = new JButton("Home");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				/*MainScreen ms = new MainScreen();
				ms.setVisible(true);*/
				MINSK m = new MINSK();
				m.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_1.setBounds(290, 122, 115, 29);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tfa.setText("");
				tfb.setText("");
				tfc.setText("");
				rootresult1.setText("");
				rootresult2.setText("");
			}
		});
		btnReset.setBounds(160, 122, 115, 29);
		frame.getContentPane().add(btnReset);
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			try{
				
				
				if(Integer.parseInt(tfa.getText())==0)
				{
					JOptionPane.showMessageDialog(null, "Value of Co-efficient a can not be 0");
				}
				else{
				int a = Integer.parseInt(tfa.getText());
				int b = Integer.parseInt(tfb.getText());
				int c = Integer.parseInt(tfc.getText());
				if(a<715827882 && b<715827882 && c<715827882)
				{
				int isPrimeSum = a+b+c;
				Prime p = new Prime(isPrimeSum);
				boolean flag = p.CheckPrime();
				if (flag == false)
				{
					JOptionPane.showMessageDialog(null, "Sum of Co-efficients should be a Prime number!!" + "\n Try again.");
					
				}
				else{
				
				String[] DisplayResult;
				/*QuadraticEquation QE = new QuadraticEquation();*/
				QuadraticSC QE = new QuadraticSC();
				DisplayResult = QE.solve(a, b, c);
				System.out.println(DisplayResult[0]);
				System.out.println(DisplayResult[1]);
				rootresult1.setText(DisplayResult[0]);
				rootresult2.setText(DisplayResult[1]);
				/*lblNewLabel_2.setText(DisplayResult);*/
				}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Value of Co-efficients should be smaller than 715827882");
				}
				}
			}catch(Exception e){JOptionPane.showMessageDialog(null, "Incorrect Data Type! Numbers Only!",
                    "Input error", JOptionPane.ERROR_MESSAGE);
            
                 return;}
			}
			
				
			
		});
	}
}
