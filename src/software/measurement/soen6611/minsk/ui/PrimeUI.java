package software.measurement.soen6611.minsk.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;

import software.measurement.soen6611.minsk.Prime;
import software.measurement.soen6611.minsk.main.MINSK;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PrimeUI {

	public JFrame frame;
	private JTextField isprime;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					PrimeUI window = new PrimeUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PrimeUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 415, 244);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblEnterTheCoefficients = new JLabel("To check for Primality:");
		lblEnterTheCoefficients.setBounds(10, 16, 403, 20);
		frame.getContentPane().add(lblEnterTheCoefficients);

		JButton btnNewButton_1 = new JButton("Home");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				/*
				 * MainScreen ms = new MainScreen(); ms.setVisible(true);
				 */
				MINSK m = new MINSK();
				m.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_1.setBounds(260, 102, 115, 29);
		frame.getContentPane().add(btnNewButton_1);

		JLabel lblEnterNumber = new JLabel("Enter Number:");
		lblEnterNumber.setBounds(10, 52, 105, 20);
		frame.getContentPane().add(lblEnterNumber);

		isprime = new JTextField();
		isprime.setBounds(126, 49, 95, 26);
		frame.getContentPane().add(isprime);
		isprime.setColumns(10);
		final JLabel result = new JLabel("");
		result.setBounds(10, 147, 350, 20);
		frame.getContentPane().add(result);

		JButton btnIsPrime = new JButton("Is Prime ?");
		btnIsPrime.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
								
				Prime p = new Prime(Integer.parseInt(isprime.getText()));
				
				if(Integer.parseInt(isprime.getText()) < 0)
				{
					JOptionPane.showMessageDialog(null, "Enter a positive number");
				}
				else{
				if (p.CheckPrime() == true) {
					result.setText("Entered number is a Prime number");
				} else {
					result.setText("Entered number is not a Prime number");
				}
				}
			}catch(Exception z) { 
                     JOptionPane.showMessageDialog(null, "Incorrect Data Type! Numbers Only!",
                        "Input error", JOptionPane.ERROR_MESSAGE);
                    
                     return;
            }
			}
		});
		btnIsPrime.setBounds(10, 102, 115, 29);
		frame.getContentPane().add(btnIsPrime);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				result.setText("");
				isprime.setText("");
			}
		});
		btnReset.setBounds(136, 102, 115, 29);
		frame.getContentPane().add(btnReset);
	}
}
