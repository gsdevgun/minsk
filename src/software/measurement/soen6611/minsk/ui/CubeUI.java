package software.measurement.soen6611.minsk.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import software.measurement.soen6611.minsk.Cubic;
import software.measurement.soen6611.minsk.OddComposite;
import software.measurement.soen6611.minsk.Prime;
import software.measurement.soen6611.minsk.main.MINSK;


public class CubeUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1411937604128399010L;
	public JFrame frame;
	private JTextField CA;
	private JTextField CB;
	private JTextField CC;
	private JTextField CD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					CubeUI window = new CubeUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CubeUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 470, 428);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton Solve = new JButton("Solve");
		
		Solve.setBounds(25, 185, 115, 29);
		frame.getContentPane().add(Solve);
		
		JLabel lblEnterTheCoefficients = new JLabel("Enter the Coefficient of a Cubic equation of the form:");
		lblEnterTheCoefficients.setBounds(10, 16, 403, 20);
		frame.getContentPane().add(lblEnterTheCoefficients);
		
		JLabel lblAxBx = new JLabel("ax^3 + bx^2+cx+d =0");
		lblAxBx.setBounds(10, 41, 403, 29);
		frame.getContentPane().add(lblAxBx);
		
		JButton btnNewButton_1 = new JButton("Home");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				/*MainScreen ms = new MainScreen();
				ms.setVisible(true);*/
				MINSK m = new MINSK();
				m.frame.setVisible(true);
				frame.dispose();
			}
		});
		btnNewButton_1.setBounds(298, 185, 115, 29);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel label = new JLabel("a:");
		label.setBounds(76, 102, 22, 20);
		frame.getContentPane().add(label);
		
		CA = new JTextField();
		CA.setColumns(10);
		CA.setBounds(113, 99, 83, 26);
		frame.getContentPane().add(CA);
		
		JLabel label_1 = new JLabel("b:");
		label_1.setBounds(231, 102, 22, 20);
		frame.getContentPane().add(label_1);
		
		CB = new JTextField();
		CB.setColumns(10);
		CB.setBounds(268, 99, 83, 26);
		frame.getContentPane().add(CB);
		
		JLabel label_2 = new JLabel("c:");
		label_2.setBounds(76, 144, 22, 20);
		frame.getContentPane().add(label_2);
		
		CC = new JTextField();
		CC.setColumns(10);
		CC.setBounds(113, 141, 83, 26);
		frame.getContentPane().add(CC);
		
		JLabel label_3 = new JLabel("d:");
		label_3.setBounds(231, 144, 22, 20);
		frame.getContentPane().add(label_3);
		
		CD = new JTextField();
		CD.setColumns(10);
		CD.setBounds(268, 141, 83, 26);
		frame.getContentPane().add(CD);
		
		JLabel label_4 = new JLabel("Root1:");
		label_4.setBounds(25, 230, 48, 20);
		frame.getContentPane().add(label_4);
		
		final JLabel R1 = new JLabel("");
		R1.setBounds(76, 230, 357, 20);
		frame.getContentPane().add(R1);
		
		JLabel lblRoot = new JLabel("Root2:");
		lblRoot.setBounds(25, 266, 48, 20);
		frame.getContentPane().add(lblRoot);
		
		final JLabel R2 = new JLabel("");
		R2.setBounds(76, 266, 357, 20);
		frame.getContentPane().add(R2);
		
		JLabel lblRoot_1 = new JLabel("Root3:");
		lblRoot_1.setBounds(25, 302, 48, 20);
		frame.getContentPane().add(lblRoot_1);
		
		final JLabel R3 = new JLabel("");
		R3.setBounds(76, 302, 357, 20);
		frame.getContentPane().add(R3);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CA.setText("");
				CB.setText("");
				CC.setText("");
				CD.setText("");
				R1.setText("");
				R2.setText("");
				R3.setText("");
				
			}
		});
		btnReset.setBounds(168, 185, 115, 29);
		frame.getContentPane().add(btnReset);
		
		JLabel lblWhereAAnd = new JLabel("Where, a!=0 and sum of a,b,c & d is odd composite number");
		lblWhereAAnd.setBounds(10, 72, 455, 29);
		frame.getContentPane().add(lblWhereAAnd);
		Solve.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			try{
			
			Cubic c = new Cubic();
			if(Double.parseDouble(CA.getText())==0 && Double.parseDouble(CB.getText())!=0)
			{	
				
				JOptionPane.showMessageDialog(null, "To Solve quadratic equation, use 'Solve Quadratic Equation' from home page");
				frame.dispose();
				MINSK m = new MINSK();
				m.frame.setVisible(true);
				
			}
			else if(Double.parseDouble(CA.getText())==0 && Double.parseDouble(CB.getText())==0 && Double.parseDouble(CB.getText())==0 && Double.parseDouble(CB.getText())==0)
			{
				JOptionPane.showMessageDialog(null, "Not a valid equation!!");
				
				
			}
			else if(CA.getText()==" "|| CB.getText()=="" || CC.getText()=="" || CD.getText()=="" )
			{
				JOptionPane.showMessageDialog(null, "Enter all the Co-efficients");
			}
			else{
				
				if(Integer.parseInt(CA.getText()) < 536870911 && Integer.parseInt(CB.getText()) < 536870911 && Integer.parseInt(CC.getText()) < 536870911 && Integer.parseInt(CD.getText()) < 536870911 )
				{
					
				
				int sum = Integer.parseInt(CA.getText()) + Integer.parseInt(CB.getText()) + Integer.parseInt(CC.getText()) + Integer.parseInt(CD.getText());
				boolean flag;
				OddComposite p = new OddComposite();
				flag = p.CheckOddComposite(sum);
				
				if(flag==true)
				{
				
					String r1 = c.realCubeRoot(Double.parseDouble(CA.getText()), Double.parseDouble(CB.getText()), Double.parseDouble(CC.getText()), Double.parseDouble(CD.getText()), 1);
					String r2 = c.realCubeRoot(Double.parseDouble(CA.getText()), Double.parseDouble(CB.getText()), Double.parseDouble(CC.getText()), Double.parseDouble(CD.getText()), 2);
					String r3 = c.realCubeRoot(Double.parseDouble(CA.getText()), Double.parseDouble(CB.getText()), Double.parseDouble(CC.getText()), Double.parseDouble(CD.getText()), 3);
					R1.setText(r1);
					R2.setText(r2);
					R3.setText(r3);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Sum of Co-efficients should be a Odd Composite number!!" + "\n Try again.");
					
				}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Value of Co-efficients should be smaller than 536870911" + "\n Try again.");
					
				}
			
			}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Incorrect Data Type! Numbers Only!",
                    "Input error", JOptionPane.ERROR_MESSAGE);
            
             }
			}
			
				
			
		});
	}
}
