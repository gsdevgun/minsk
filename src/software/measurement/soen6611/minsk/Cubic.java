package software.measurement.soen6611.minsk;

import software.measurement.soen6611.minsk.maths.SqRoot;

public class Cubic {
	
	// Uses Newton-Rapson's method
			SqRoot sqrt = SqRoot.getInstance();
			
			double root;

			public String realCubeRoot(double a, double b, double c, double d, int n) {
				if (a == 0) {
					return "To Solve quadratic equation, use 'Solve Quadratic Equation' from home page";

				} else {
					
					
					double xnew = findRoot1(a,b,c,d);
					String root1 = Double.toString(xnew);
					
					double a1 = a;
					double b1 = a1*xnew + b;
					double c1 = b1*xnew + c;
					QuadraticSC qsc = new QuadraticSC();
					String[] qroots = qsc.solve(a1, b1, c1);
					
					if(n==1)
					{
						return root1;
					}
					else if(n==2)
					{
						return qroots[0];
					}
					else if(n==3)
					{
						return qroots[1];
					}
				
									
				}
				return "No Root Found";
			}
		
			public double findRoot1(double a, double b, double c, double d)
			{
				double xold = 3;
				int iter = 0;
				double Err;
				double absErr;
				double xnew;
				do {
					iter = iter + 1;
					double f = (a * (xold * xold * xold) + b * (xold * xold) + c * xold + d);
					double df = (3 * a * (xold * xold) + 2 * b * xold + c);
					xnew = xold - f / df;
					Err = xnew - xold;
					xold = xnew;
					//Calculate Math.abs on Err
					if(Err < 0)
						absErr = -Err;
					else
						absErr = Err;
					
				} while (iter < 1000000000 && absErr > 0.000001);
				
				return xnew;
			}

	
}