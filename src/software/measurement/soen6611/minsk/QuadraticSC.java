package software.measurement.soen6611.minsk;

import software.measurement.soen6611.minsk.maths.Sgn;
import software.measurement.soen6611.minsk.maths.SqRoot;

public class QuadraticSC {
	
	String[] roots = new String[2];
	double r1,r2;
	Sgn sgn = Sgn.getInstance();
	SqRoot sqrt = SqRoot.getInstance();
	
	public String[] solve(double a, double b, double c) {
		
		
		double D;
		D = b * b - 4 * a * c;
		System.out.println("Value of D is: " + D);
		
		roots = calculateRoots(a,b,c,D);
		System.out.println(roots[0]);
		System.out.println(roots[1]);
		
		return roots;
		}
	
	public String[] calculateRoots(double a, double b, double c, double D)
	{
		if (D == 0) {
			
			roots[0] = Double.toString((-b / 2 * a));
			roots[1] = "No Second Root";
			return roots;
		} else if (D > 0) {
			if(b==0)
			{	
				 double Disc = sqrt.SquareRoot(- 4*a*c);
				 r1 = (Disc) / (2 * a);
				 r2 = (- Disc) / (2 * a);
				 
							
			}
			else
			{
			 r1 = (-b - sgn.checkSignum(b)* sqrt.SquareRoot(D))/(2 * a);
			 r2 = (2*c)/(-b - sgn.checkSignum(b) * sqrt.SquareRoot(D));
			 
			}
			roots[0] = Double.toString(r1);
			roots[1] = Double.toString(r2);
			return roots;
		} else {
			if(b==0)
			{	
				 double Disc = sqrt.SquareRoot(-D);
				 r1 = (Disc) / (2 * a);
				 r2 = (- Disc) / (2 * a);
				 
				 String imag1 = "+"+r1 + "i";
				 String imag2 = "-"+r1 + "i";
				 roots[0] = imag1;
				 roots[1] = imag2;
				 System.out.println(roots[0]);
				 System.out.println(roots[1]);
								
			}
			else{
			r1 = sgn.checkSignum(b) * sqrt.SquareRoot(-D)/ (2 * a);
			r2 = sgn.checkSignum(b) * sqrt.SquareRoot(-D );
			
			double r2c = 2 * c;
			String root1,root2=null;
			root1 = -b/(2*a) + " - (" + r1 + " i)";
			root2 = r2c + "/ ( " + -b + " - (" + r2 + " i) )";
			roots[0] = root1;
			roots[1] = root2;
			}
			return roots;
		}
		
	}
}
	


