package software.measurement.soen6611.minsk;

import software.measurement.soen6611.minsk.maths.SqRoot;

/**
 * Created by Hardik on 2016-05-14.
 */
public class Prime {
	SqRoot sqrt = SqRoot.getInstance();
    int number;
    boolean isPrime;

    public Prime(int number) {
        this.number = number;
    }

   
    public boolean CheckPrime()
    {
        if(this.number==1)
            return false;
        if(this.number==2 || this.number == 3)
            return true;
        if (this.number % 2 == 0 || this.number % 3 == 0)
			return false;
        for (int k = 1; ; k++) {
			
			if (this.number % (6 * k + 1) == 0 || this.number % (6 * k - 1) == 0)
				return false;
			else
				return true;
		}
                
        
    }
    
   
    
    
}
