package software.measurement.soen6611.minsk.main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

import software.measurement.soen6611.minsk.ui.CubeUI;
import software.measurement.soen6611.minsk.ui.PrimeUI;
import software.measurement.soen6611.minsk.ui.QuadraticUI;

import java.awt.Font;

public class MINSK {

	public JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MINSK window = new MINSK();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MINSK() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnSolveQuadraticEquation = new JButton("Solve Quadratic Equation");
		btnSolveQuadraticEquation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				QuadraticUI quadratic = new QuadraticUI();
				quadratic.frame.setVisible(true);
			}
		});
		btnSolveQuadraticEquation.setBounds(109, 71, 249, 29);
		frame.getContentPane().add(btnSolveQuadraticEquation);

		JButton btnSolvePrimeNumber = new JButton("Solve Prime Number");
		btnSolvePrimeNumber.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				PrimeUI prime = new PrimeUI();
				prime.frame.setVisible(true);
			}
		});
		btnSolvePrimeNumber.setBounds(109, 116, 249, 29);
		frame.getContentPane().add(btnSolvePrimeNumber);

		JButton btnSolveCubicEquation = new JButton("Solve Cubic Equation");
		btnSolveCubicEquation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				CubeUI cmu = new CubeUI();
				cmu.frame.setVisible(true);

			}
		});
		btnSolveCubicEquation.setBounds(109, 161, 249, 29);
		frame.getContentPane().add(btnSolveCubicEquation);

		JLabel lblMinsk = new JLabel("MINSK");
		lblMinsk.setFont(new Font("Tahoma", Font.PLAIN, 52));
		lblMinsk.setBounds(151, 0, 185, 63);
		frame.getContentPane().add(lblMinsk);
	}

}
